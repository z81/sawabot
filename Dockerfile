FROM node:10-alpine

WORKDIR /server

COPY . /server
RUN npm install
CMD npm start