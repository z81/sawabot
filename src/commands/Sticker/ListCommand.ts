import { Command } from "../../Command";
import { CmdContent } from "../../ICmdContent";
import { StickerStore } from "./StickerStore";
import { StickerCommand } from "./StickerCommand";

const wrapMessage = (str: string) => str.match(/.{1,1995}[ \r\n\t]?/gim);

export class ListCommand extends Command {

    public antispamTime = 60;

    constructor(stickersList: StickerStore) {
        super();
        this.stickers = stickersList;
    }

    public exec(argument: Array<string>, ctx: CmdContent): boolean {
        const stickers = this.stickers.getAll();
        const { send } = ctx.message.channel;

        const text = ` **Список стикеров:**  ${stickers
            .map((s, i) => `**${i + 1}** ${s.name}`)
            .join(", ")}`;

        const messages = wrapMessage(text);

        if (messages) {

            messages.forEach(msg =>
                ctx.message.channel.send(StickerCommand.removeNicknames(msg))
            );
        }

        return true;
    }

    protected name = "лист";

    protected description = "";

    private stickers: StickerStore;
}