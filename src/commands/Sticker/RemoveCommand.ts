import { Command } from "../../Command";
import { CmdContent } from "../../ICmdContent";
import { StickerStore } from "./StickerStore";
import { StickerCommand } from "./StickerCommand";

const wrapMessage = (str: string) => str.match(/.{1,1995}[ \r\n\t]?/gim);

export class RemoveCommand extends Command {
    constructor(stickersList: StickerStore) {
        super();
        this.stickers = stickersList;
    }

    public exec(argument: Array<string>, ctx: CmdContent): boolean {
        try {
            this.remove(argument.join(" "), ctx);
        } catch (e) {
            ctx.message.reply("Произошла ошибка")
        }

        return true;
    }

    private async remove(name: string, ctx: CmdContent) {
        try {
            await this.stickers.removeByName(name);
            ctx.message.reply("Стикер удален");
        } catch (e) {
            console.error(e);
            ctx.message.reply("Произошла ошибка");
        }
    }

    protected name = "удалить";

    protected description = "";

    private stickers: StickerStore;
}