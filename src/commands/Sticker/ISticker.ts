export interface ISticker {
    name: string,
    ext: string,
    path: string
}