import { Command } from "../../Command";
import { ListCommand } from "./ListCommand";
import { RemoveCommand } from "./RemoveCommand";
import { StickerStore } from "./StickerStore";
import { CmdContent } from "../../ICmdContent";
import { ISticker } from "./ISticker";

export class StickerCommand extends Command {
  protected name: string = "стикер";

  protected description: string = "";

  private stickers = new StickerStore(`${__dirname}/../../../data/stickers`);

  protected subCommands = [];

  constructor() {
    super();

    this.addCommand(new ListCommand(this.stickers));
    this.addCommand(new RemoveCommand(this.stickers));
  }

  public exec(args: Array<string>, ctx: CmdContent): boolean {
    const isNotFoundCommand = !super.exec(args, ctx);

    if (isNotFoundCommand) {
      const stickerName = args.join(" ");
      const attach = ctx.message.attachments.first();

      if (attach) {
        const sticker = this.stickers.findOne(stickerName, 8);

        if (sticker) {
          ctx.message.reply(`Стикер именем ${stickerName} уже загружен`);
        } else {
          this.uploadSticker(stickerName, attach.url, ctx);
        }
      } else {
        const sticker = this.stickers.findOne(stickerName);
        StickerCommand.sendSticker(sticker, ctx);
      }
    }

    ctx.message.delete();

    return true;
  }

  private async uploadSticker(
    name: string,
    url: string,
    { message }: CmdContent
  ) {
    if (!StickerCommand.isImage(url)) {
      return void message.reply("Это не изображение");
    }

    try {
      await this.stickers.addFromUrl(name, url);
      message.reply(` добавил стикер "${name}"`);
    } catch (e) {
      console.error(e);
      message.reply("Произошла ошибка");
    }
  }

  private static sendSticker(
    sticker: ISticker | null,
    { message }: CmdContent
  ) {
    if (sticker) {
      const name = StickerCommand.removeNicknames(sticker.name);
      const text = `<@${message.author.id}>, ${name}`;
      const attach = { files: [sticker.path] };

      message.channel.send(text, attach);
    } else {
      message.reply("Стикер не найден");
    }
  }

  public static removeNicknames(text: string) {
    return text.replace(/<@(.*)>/gim, "");
  }

  private static isImage(filename: string) {
    return !!/\.(gif|jpe?g|tiff|png)$/.test(filename);
  }
}
