import * as fs from "fs";
import * as path from "path";
import * as https from "https";
import { promisify } from "util";
import { DocumentIndex } from "ndx";
import { ISticker } from "./ISticker";

const readDirAsync = promisify(fs.readdir);

const NWSTART_RE = /(^[a-zA-Zа-яА-ЯёЁ])+/;

export class StickerStore {
  private path: string = "";

  private stickers: ISticker[] = [];

  private ignoredNames = [".gitkeep"];

  private fuse = new DocumentIndex({
    filter(text: string) {
      return text.toLowerCase().replace(NWSTART_RE, "");
    }
  });

  constructor(path: string) {
    this.path = path;
    this.fuse.addField("name");
    this.init();
  }

  private async init() {
    await this.load();

    this.stickers.forEach((sticker, i) => {
      this.fuse.add(i, sticker);
    });
  }

  private async load() {
    const files = await readDirAsync(this.path);

    this.stickers = files
      .filter(name => this.ignoredNames.indexOf(name) === -1)
      .map(filename => {
        const ext = path.extname(filename);
        const name = path.basename(filename, ext);
        const path_ = `${this.path}/${filename}`;

        return {
          path: path_,
          name,
          ext
        };
      });
  }

  public findOne(name: string, minScore?: number): ISticker | null {
    if (!this.fuse) return null;

    const result = this.fuse.search(name);

    if (result.length && (!minScore || result[0].score > minScore)) {
      return this.stickers[(result[0] as any).docId];
    }

    return null;
  }

  public getAll() {
    return this.stickers;
  }

  public async addFromUrl(name: string, url: string) {
    const ext = path.extname(url);
    const dist = `${this.path}/${name}${ext}`;

    await this.download(url, dist);
    const sticker = {
      name,
      path: dist,
      ext
    };
    this.stickers.push(sticker);
    this.fuse.add(this.stickers.length - 1, sticker);
  }

  private download(url: string, dist: string) {
    return new Promise((resolve, reject) => {
      const file = fs.createWriteStream(dist);

      const request = https.get(url, r => {
        r.pipe(file);

        file.on("finish", () => {
          file.close();
          resolve();
        });
      });

      request.on("error", reject);
    });
  }

  public removeByName(name: string) {
    const result = this.stickers.filter(s => s.name === name);

    if (result.length === 0) {
      throw Error("Not found");
    }

    return this.remove(result[0]);
  }

  public remove(sticker: ISticker) {
    return new Promise((resolve, reject) =>
      fs.unlink(sticker.path, err => {
        if (err) {
          reject(err);
        } else {
          const idx = this.stickers.indexOf(sticker);
          this.stickers.splice(idx, 1);

          resolve();
        }
      })
    );
  }
}
