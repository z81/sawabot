import { Command } from "../../Command";
import { CmdContent } from "../../ICmdContent";

export class PovalilosCommand extends Command {
  protected name: string = "повалилось";

  protected description: string = "";

  public exec(args: Array<string>, ctx: CmdContent): boolean {
    if (!args.length) return false;

    const text = `У ${args.join(" ")} ПОВАЛИЛОСЬ`;
    ctx.message.channel.send(text);

    return true;
  }
}
