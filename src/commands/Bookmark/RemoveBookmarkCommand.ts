import { Command } from "../../Command";
import { BookmarksStore } from "./BookmarksStore";
import { CmdContent } from "../../ICmdContent";

export class RemoveBookmarkCommand extends Command {
  protected name: string = "удалить";

  protected description: string = "";

  private store: BookmarksStore;

  constructor(store: BookmarksStore) {
    super();
    this.store = store;
  }

  public exec(args: Array<string>, ctx: CmdContent): boolean {
    const name = args.join(" ");
    const { bookmarks } = this.store.data;

    if (name && bookmarks[name]) {
      this.store.remove(name);
      ctx.message.reply("Закладка удалена");
      return true;
    } else {
      ctx.message.reply("Закладка не найдена");
    }

    return true;
  }
}
