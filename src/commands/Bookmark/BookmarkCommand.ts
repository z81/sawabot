import { Command } from "../../Command";
import { BookmarksStore } from "./BookmarksStore";
import { AddBookmarkCommand } from "./AddBookmarkCommand";
import { RemoveBookmarkCommand } from "./RemoveBookmarkCommand";
import { CmdContent } from "../../ICmdContent";

export class BookmarkCommand extends Command {
  protected name: string = "закладка";

  protected description: string = "";

  private store = new BookmarksStore(
    `${__dirname}/../../../data/bookmarks.json`
  );

  constructor() {
    super();

    this.addCommand(new AddBookmarkCommand(this.store));
    this.addCommand(new RemoveBookmarkCommand(this.store));
  }

  public exec(args: Array<string>, ctx: CmdContent): boolean {
    const isNotFoundCommand = !super.exec(args, ctx);

    if (isNotFoundCommand) {
      const name = args.join(" ");
      const { bookmarks } = this.store.data;

      if (name && bookmarks[name]) {
        ctx.message.reply(bookmarks[name].value);
      } else {
        ctx.message.reply("Закладка не найдена");
      }
    }

    return true;
  }
}
