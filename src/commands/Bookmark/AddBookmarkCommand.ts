import { Command } from "../../Command";
import { BookmarksStore } from "./BookmarksStore";
import { CmdContent } from "../../ICmdContent";

export class AddBookmarkCommand extends Command {
  protected name: string = "добавить";

  protected description: string = "";

  private store: BookmarksStore;

  constructor(store: BookmarksStore) {
    super();
    this.store = store;
  }

  public exec(args: Array<string>, ctx: CmdContent): boolean {
    const name = args.shift();
    const data = args.join(" ");
    const { bookmarks } = this.store.data;

    if (name && bookmarks[name]) {
      ctx.message.reply("Закладка уже существует");
      return true;
    }

    if (name && data) {
      this.store.setValue(name, data);
      ctx.message.reply("Закладка добавлена");
    } else {
      ctx.message.reply("Недостаточно параметров");
    }

    return true;
  }
}
