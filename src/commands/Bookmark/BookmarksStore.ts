import fs from "fs";
import { promisify } from "util";
import { IBookmarkData } from "./IBookmarkData";

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);

export class BookmarksStore {
  private storePath: string = "";

  private _data: IBookmarkData = {
    bookmarks: {}
  };

  constructor(storePath: string) {
    this.storePath = storePath;
    this.load(storePath);
  }

  public async save() {
    try {
      const data = JSON.stringify(this.data);
      await writeFileAsync(this.storePath, data, {
        flag: "w",
        encoding: "utf8"
      });
    } catch (e) {
      console.error(e);
    }
  }

  public get data(): IBookmarkData {
    return this._data;
  }

  public setValue(name: string, value: string) {
    this.data.bookmarks[name] = { value };
    this.save();
  }

  public remove(name: string) {
    delete this.data.bookmarks[name];
    this.save();
  }

  private async load(storePath: string) {
    try {
      const data = await readFileAsync(storePath);
      this._data = JSON.parse(data.toString("utf-8") || "{}");
    } catch (e) {
      console.error(e);
    }
  }
}
