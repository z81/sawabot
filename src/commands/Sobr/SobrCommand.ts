import { Command } from "../../Command";
import { CmdContent } from "../../ICmdContent";

export class SobrCommand extends Command {

    protected name: string = "собр";

    protected description: string = "";

    public exec(args: Array<string>, ctx: CmdContent): boolean {
        if (!args.length) return false;

        const text = `За ${args.join(" ")} выехал собр`;
        ctx.message.channel.send(text);

        return true;
    }
}