export default {
  token: "",
  permissions: {
    child: {
      стикер: {
        allow: {
          roles: ["Elite Dangerous", "stickers"]
        },
        child: {
          удалить: {
            allow: {
              ids: ["113324999913279488"]
            }
          },
          upload: {
            disallow: {
              roles: ["noupload"]
            }
          }
        }
      }
    }
  }
};
