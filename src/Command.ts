import { CmdContent } from "./ICmdContent";
import { Message } from "discord.js";

export class Command {
  public antispamTime = 0;

  public callTime = 0;

  protected name = "";

  protected description = "";

  protected subCommands: any[] = [];

  protected permissions: any = {};

  static parse(message: string) {
    return message.split(" ").filter(c => c !== "");
  }

  public test(text: string) {
    return text.toLowerCase() === this.name;
  }

  public exec(args: string[], ctx: CmdContent): boolean {
    if (!args.length) return false;

    for (const cmd of this.subCommands) {
      if (cmd.test(args[0])) {
        if (!this.checkPermissions(cmd.getPermissions(), ctx.message)) {
          ctx.message.reply("Недостаточно прав");
          return true;
        }

        if (cmd.callTime + cmd.antispamTime * 1000 > Date.now()) {
          ctx.message.reply("Нельзя вызывать команду так часто");
          return true;
        }

        cmd.exec(args.splice(1), ctx);
        cmd.callTime = Date.now();

        return true;
      }
    }

    return false;
  }

  public addCommand(cmd: Command) {
    this.subCommands.push(cmd);
  }

  public setPermissions(permissions: object) {
    this.permissions = permissions;

    for (const cmd of this.subCommands) {
      const permissions = this.permissions as any;
      const childPerm =
        permissions && permissions.child ? permissions.child[cmd.name] : {};

      cmd.setPermissions(childPerm);
    }
  }

  public getPermissions() {
    return this.permissions;
  }

  private checkPermissions(perms: any, message: Message) {
    if (!perms) return true;

    let isAllowed = true;

    if (perms.allow) {
      isAllowed = false;

      if (
        perms.allow.roles &&
        this.isIncludeRoles(perms.allow.roles, message.member.roles)
      ) {
        isAllowed = true;
      }

      if (perms.allow.ids && perms.allow.ids.indexOf(message.author.id) !== -1) {
        isAllowed = true;
      }
    }

    if (perms.disallow) {
      if (
        perms.disallow.roles &&
        this.isIncludeRoles(perms.disallow.roles, message.member.roles)
      ) {
        isAllowed = false;
      }

      if (perms.disallow.ids && perms.disallow.ids.indexOf(message.author.id) !== -1) {
        isAllowed = false;
      }
    }

    return isAllowed;
  }

  private isIncludeRoles(roles: string[], discordRoles: any) {
    return roles.some((role: string) => discordRoles.exists("name", role));
  }
}
