import { StickerCommand } from "./commands/Sticker/StickerCommand";
import { SobrCommand } from "./commands/Sobr/SobrCommand";
import { PovalilosCommand } from "./commands/Povalilos/PovalilosCommand";
import { BookmarkCommand } from "./commands/Bookmark/BookmarkCommand";
import { Command } from "./Command";
import Discord, { TextChannel } from "discord.js";
import config from "../data/config";
import { getNewCommits } from "./gitlog";

const root = new Command();
root.addCommand(new StickerCommand());
root.addCommand(new SobrCommand());
root.addCommand(new PovalilosCommand());
root.addCommand(new BookmarkCommand());
root.setPermissions(config.permissions);

const client = new Discord.Client();

client.on("ready", async () => {
  client.user.setActivity("Здарова бандиты", {
    type: "WATCHING"
  });

  const newCommits = await getNewCommits();
  const message = `Здарова бандиты! Я вернулся!\nСписок изменений: \n ${newCommits
    .map((c: any) => c.subject)
    .join("\n")}`;

  const guild = client.guilds.get("271410547113721857"); // client.guilds.get("113325694687117312") ||

  if (guild) {
    const { channels } = guild;
    const ch = channels.get("312682116657840141") as TextChannel; // (channels.get("113325694687117312") as TextChannel) ||

    ch.send(message);
  }

  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", message => {
  root.exec(Command.parse(message.content), { message });
});

// hearth check
require("http")
  .createServer((_: any, r: any) => r.end("ok"))
  .listen(3001);

client.login(config.token);
