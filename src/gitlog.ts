import fs from "fs";
const gitlog: any = require("gitlog");

export const getNewCommits = (): Promise<any[]> =>
  new Promise((resolve, reject) => {
    gitlog({ repo: "." }, (err: any, commits: any[]) => {
      if (err) return reject(err);

      const newCommits: any[] = commits
        .filter(commit => {
          const { ctime } = fs.statSync(commit.files[0]);

          return +ctime + 1e3 * 60 * 10 > Date.now();
        })
        .filter((_, i) => i < 3);

      resolve(newCommits);
    });
  });
